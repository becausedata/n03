'use strict';

var _ = require('underscore');

var data = (function () {
  var tables = {
    prefectures: [
      {id: 'hokkaido', value: '01'},
      {id: 'aomori', value: '02'},
      {id: 'iwate', value: '03'},
      {id: 'miyagi', value: '04'},
      {id: 'akita', value: '05'},
      {id: 'yamagata', value: '06'},
      {id: 'fukushima', value: '07'},
      {id: 'ibaraki', value: '08'},
      {id: 'tochigi', value: '09'},
      {id: 'gunma', value: '10'},
      {id: 'saitama', value: '11'},
      {id: 'chiba', value: '12'},
      {id: 'tokyo', value: '13'},
      {id: 'kanagawa', value: '14'},
      {id: 'niigata', value: '15'},
      {id: 'toyama', value: '16'},
      {id: 'ishikawa', value: '17'},
      {id: 'fukui', value: '18'},
      {id: 'yamanashi', value: '19'},
      {id: 'nagano', value: '20'},
      {id: 'gifu', value: '21'},
      {id: 'shizuoka', value: '22'},
      {id: 'aichi', value: '23'},
      {id: 'mie', value: '24'},
      {id: 'shiga', value: '25'},
      {id: 'kyoto', value: '26'},
      {id: 'osaka', value: '27'},
      {id: 'hyogo', value: '28'},
      {id: 'nara', value: '29'},
      {id: 'wakayama', value: '30'},
      {id: 'tottori', value: '31'},
      {id: 'shimane', value: '32'},
      {id: 'okayama', value: '33'},
      {id: 'hiroshima', value: '34'},
      {id: 'yamaguchi', value: '35'},
      {id: 'tokushima', value: '36'},
      {id: 'kagawa', value: '37'},
      {id: 'ehime', value: '38'},
      {id: 'kochi', value: '39'},
      {id: 'fukuoka', value: '40'},
      {id: 'saga', value: '41'},
      {id: 'nagasaki', value: '42'},
      {id: 'kumamoto', value: '43'},
      {id: 'oita', value: '44'},
      {id: 'miyazaki', value: '45'},
      {id: 'kagoshima', value: '46'},
      {id: 'okinawa', value: '47'}
    ],
    years: [
      {id: 1919, value: '200101'},
      {id: 1950, value: '501001'},
      {id: 1955, value: '551001', exclude: '47'},
      {id: 1960, value: '601001', exclude: '47'},
      {id: 1965, value: '651001', exclude: '47'},
      {id: 1970, value: '701001', exclude: '47'},
      {id: 1975, value: '751001'},
      {id: 1980, value: '801001'},
      {id: 1985, value: '851001'},
      {id: 1995, value: '951001'},
      {id: 2000, value: '001001'},
      {id: 2005, value: '05'},
      {id: 2006, value: '06'},
      {id: 2007, value: '071001'},
      {id: 2008, value: '090320'},
      {id: 2009, value: '100329'},
      {id: 2010, value: '110331'},
      {id: 2011, value: '120331'},
      {id: 2012, value: '120401'},
      {id: 2013, value: '130401'}
    ]
  };

  return {
    values: function (table, key) {
      var vals = [];
      tables[table].forEach(function (obj) {
        vals.push(obj[key])
      });
      return vals;
    },

    select: function (table, id) {
      return _.find(tables[table], function (obj) {
        return obj.id === id
      });
    }
  }
}());

module.exports = data;
