'use strict';

var Q = require('q'),
    fs = require('fs'),
    request = require('request'),
    exec = require('child_process').exec,
    mkdirp = require('mkdirp'),
    glob = require('glob');

function Source() {}

Source.prototype.download = function() {
  var that = this,
      deferred = Q.defer();

  fs.exists(this.filepath, function (exists) {
    if (!exists) {
      console.info('downloading ' + that.filename);
      request(that.fileurl)
        .pipe(fs.createWriteStream(that.filepath))
        .on('close', function () { deferred.resolve(); });
    } else {
      console.info('using cached ' + that.filename);
      deferred.resolve();
    }
  });

  return deferred.promise;
};

Source.prototype.unpack = function () {
  var deferred = Q.defer(),
      tar = exec('tar', ['-xvf', this.filepath, '-C', this.filedir]);

  mkdirp(this.filedir);

  console.info('unpacking ' + this.filename);

  tar.on('close', function () {
    deferred.resolve();
  });

  return deferred.promise;
};

Source.prototype.find = function (ext) {
  var deferred = Q.defer();

  glob(this.filedir + '/*.' + ext, function (error, files) {
    deferred.resolve(files[0]);
  });

  return deferred.promise;
};

Source.prototype.transform = function () {
  var output = this.filedir + '.json';

  this.find('shp')
    .then(function (input) {
      console.info('transforming ' + input + ' to ' + output);

      return exec(['topojson -o', output, input].join(' '), function (error) {
        if (error !== null) {
          console.error('error: transform failed');
        }
      });
    });
};

module.exports = Source;
