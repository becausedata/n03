'use strict';

var util = require('./util');

var n03 = function (options) {
  if (!options.year) {
    options.year = 2013;
  }

  var year = util.year(options.year);

  if (!options.prefectures) {
    options.prefectures = util.prefectures;
  }

  var prefectures = options.prefectures.map(util.prefecture);

  if (!options.transformation) {
    options.transformation = 'topojson';
  }

  var transformation = options.transformation;

  util.sources(year, prefectures, transformation)
    .forEach(function (source) {
      if (source.message) {
        console.warn(source.message);
      } else {
        source.download()
          .then(function () { return source.unpack() })
          .then(function () { return source.transform() });
      }
    });
};

module.exports = n03;
