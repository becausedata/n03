'use strict';

var fs = require('fs'),
    path = require('path'),
    cache = path.join(path.dirname(fs.realpathSync(__filename)), '../cache'),
    lib = path.join(path.dirname(fs.realpathSync(__filename)), '../lib'),
    data = require(lib + '/data'),
    Source = require(lib + '/source');

var util = {
  years: data.values('years', 'id'),

  prefectures: data.values('prefectures', 'id'),

  year: function (id) {
    return data.select('years', id);
  },

  prefecture: function (id) {
    return data.select('prefectures', id);
  },

  sources: function (year, prefectures, transformation) {
    return prefectures.map(function (prefecture) {
      if (prefecture.value === year.exclude) {
        return Object.create({}, {
          message: { value: ['no data for ', prefecture.id, ' in ', year.id].join('') }
        });
      } else {
        var filename = ['N03-', year.value, '_', prefecture.value, '_GML.zip'].join(''),
            baseurl = 'http://nlftp.mlit.go.jp/ksj/gml/data/N03/',
            fileyy = year.id.toString().slice(2);

        return Object.create(Source.prototype, {
          filename: { value: filename },
          filepath: { value: [cache, '/', filename].join('') },
          filedir: { value: filename.slice(0, -4) },
          fileurl: { value: [baseurl, 'N03-', fileyy, '/', filename].join('') },
          transformation: { value: transformation }
        });
      }
    });
  }
};

module.exports = util;
