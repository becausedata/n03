#!/usr/bin/env node

var nomnom = require('nomnom'),
    fs = require('fs'),
    path = require('path'),
    lib = path.join(path.dirname(fs.realpathSync(__filename)), '../lib/'),
    n03 = require(lib + 'n03'),
    util = require(lib + 'util');

var options = nomnom.options({
  version: {
    abbr: 'v',
    flag: true,
    callback: function() {
      return '0.0.0';
    }
  },

  year: {
    abbr: 'y',
    metavar: 'YEAR',
    help: 'dataset year in yyyy format (e.g., 2013)',
    choices: util.years,
    default: 2013
  },

  prefectures: {
    abbr: 'p',
    metavar: 'PREFECTURE',
    help: 'prefecture name; repeatable (e.g. -p tokyo -p shizuoka)',
    choices: util.prefectures,
    default: 'hokkaido...okinawa',
    list: true
  },

  transformation: {
    abbr: 't',
    metavar: 'TRANSFORMATION',
    help: 'transformation to apply to the downloaded shape file',
    choices: ['topojson'],
    default: 'topojson'
  }
})
.script('n03')
.parse();

n03(options);
