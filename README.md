# n03
A CLI for extracting and transforming [N03](http://nlftp.mlit.go.jp/ksj/gml/datalist/KsjTmplt-N03.html), an administrative and coastal boundary data set from Japan's National Land Information Division, National Spatial Planning and Regional Policy Bureau, Ministry of Land, Infrastructure and Transport (MLIT).

## Getting Started
_(Coming soon)_

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
_(Coming soon)_

## Release History
_(Nothing yet)_

## License
Copyright (c) 2014 Søren Jones. Licensed under the MIT license.
